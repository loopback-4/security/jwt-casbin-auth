// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {registerAuthenticationStrategy} from '@loopback/authentication';
import {
  Application,
  Binding,
  Component,
  CoreBindings,
  inject,
} from '@loopback/core';
import {
  DeviceServiceBindings,
  DeviceTokenServiceBindings,
  DeviceTokenServiceConstants,
} from './declarations';

import {
    DeviceServiceImpl,
    DeviceTokenServiceImpl,
} from './services';

import {JwtAuthenticationDeviceStrategy} from './strategies';

export class JwtAuthenticationDeviceComponent implements Component {
  bindings: Binding[] = [
    
    // device bindings
    Binding.bind(DeviceServiceBindings.DEVICE_SERVICE).toClass(DeviceServiceImpl),

    // token bindings
    Binding.bind(DeviceTokenServiceBindings.TOKEN_SECRET).to(
      DeviceTokenServiceConstants.TOKEN_SECRET_VALUE,
    ),
    Binding.bind(DeviceTokenServiceBindings.TOKEN_EXPIRES_IN).to(
      DeviceTokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
    ),
    Binding.bind(DeviceTokenServiceBindings.TOKEN_SERVICE).toClass(DeviceTokenServiceImpl),

  ];
  constructor(@inject(CoreBindings.APPLICATION_INSTANCE) app: Application) {
    registerAuthenticationStrategy(app, JwtAuthenticationDeviceStrategy);
  }
}
