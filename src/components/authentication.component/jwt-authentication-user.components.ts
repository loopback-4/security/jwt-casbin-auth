// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {registerAuthenticationStrategy} from '@loopback/authentication';
import {
  Application,
  Binding,
  Component,
  CoreBindings,
  inject,
} from '@loopback/core';
import {
  UserRefreshTokenConstants,
  UserRefreshTokenServiceBindings,
  UserTokenServiceBindings,
  UserTokenServiceConstants,
  UserServiceBindings,
} from './declarations';
import {
  UserServiceImpl, 
  UserRefreshTokenServiceImpl,
  UserTokenServiceImpl,
} from './services';
import {JwtAuthenticationUserStrategy} from './strategies';

export class JwtAuthenticationUserComponent implements Component {
  bindings: Binding[] = [
    
    // user bindings
    Binding.bind(UserServiceBindings.USER_SERVICE).toClass(
      UserServiceImpl
    ),

    // token bindings
    Binding.bind(UserTokenServiceBindings.TOKEN_SERVICE).toClass(
      UserTokenServiceImpl
    ),
    Binding.bind(UserTokenServiceBindings.TOKEN_SECRET).to(
      UserTokenServiceConstants.TOKEN_SECRET_VALUE,
    ),
    Binding.bind(UserTokenServiceBindings.TOKEN_EXPIRES_IN).to(
      UserTokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
    ),
    

    // refresh token bindings
    Binding.bind(UserRefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE).toClass(
      UserRefreshTokenServiceImpl,
    ),
    Binding.bind(UserRefreshTokenServiceBindings.REFRESH_SECRET).to(
      UserRefreshTokenConstants.REFRESH_SECRET_VALUE,
    ),
    Binding.bind(UserRefreshTokenServiceBindings.REFRESH_EXPIRES_IN).to(
      UserRefreshTokenConstants.REFRESH_EXPIRES_IN_VALUE,
    ),
    Binding.bind(UserRefreshTokenServiceBindings.REFRESH_ISSUER).to(
      UserRefreshTokenConstants.REFRESH_ISSUER_VALUE,
    ),
  ];
  constructor(@inject(CoreBindings.APPLICATION_INSTANCE) app: Application) {
    registerAuthenticationStrategy(app, JwtAuthenticationUserStrategy);
  }
}
