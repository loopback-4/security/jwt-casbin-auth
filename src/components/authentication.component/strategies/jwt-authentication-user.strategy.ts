// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { AuthenticationStrategy } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { Request } from '@loopback/rest';
import { UserProfile } from '@loopback/security';
import { UserTokenServiceBindings } from '../declarations';
import { UserTokenServiceImpl } from '../services';

export class JwtAuthenticationUserStrategy implements AuthenticationStrategy {
  name = 'jwt-user';

  constructor(
    @inject(UserTokenServiceBindings.TOKEN_SERVICE) public tokenService: UserTokenServiceImpl,
  ) { }

  async authenticate(request: Request): Promise<UserProfile | undefined> {
    const token: string = await this.tokenService.extractCredentials(request);
    let userProfile: UserProfile = await this.tokenService.verifyToken(token);
    return userProfile;
  }

}
