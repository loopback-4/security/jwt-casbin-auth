// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Credentials, UserService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {BcryptHasherBindings, BcryptHasherService} from '../..';
import {
  Device,
  Role
} from '../../../models';
import {
  DeviceRepository,
  RbacRepository
} from '../../../repositories';



export class DeviceServiceImpl implements UserService<Device, Credentials> {
  constructor(
    @repository(DeviceRepository)
    public deviceRepository: DeviceRepository,
    @repository(RbacRepository)
    public rbacRepository: RbacRepository,
    @inject(BcryptHasherBindings.BCRYPT_HASHER_SERVICE)
    private bcryptHasherService: BcryptHasherService,
  ) { }
  registerAdmin(user: Device): Promise<Device> {
    throw new Error('Method not implemented.');
  }
  registerOfficer(user: Device): Promise<Device> {
    throw new Error('Method not implemented.');
  }
  registerMarshal(user: Device): Promise<Device> {
    throw new Error('Method not implemented.');
  }

  async findUserById(id: number): Promise<Device> {
    const deviceNotfound = 'invalid Device';
    const foundDevice = await this.deviceRepository.findOne({
      where: {id: id},
    });

    if (!foundDevice) {
      throw new HttpErrors.Unauthorized(deviceNotfound);
    }
    return foundDevice;
  }

  async findUserRoleById(deviceId: number): Promise<Role[]> {
    const rbacs = await this.rbacRepository.find({
      where: {and: [{principalType: 'Device'}, {principalId: deviceId}]},
      include: [{relation: 'role'}]
    });

    let roles: Role[] = [];
    for (const rbac of rbacs) {
      const rbacJson = rbac.toJSON() as any;
      if (rbacJson.role) {
        let role = rbacJson.role;
        delete role.created;
        delete role.description;
        delete role.modified;
        roles.push(role);
      }
    }
    return roles;
  }

  async verifyCredentials(credentials: Credentials): Promise<Device> {
    const invalidCredentialsError = 'Invalid username or password.';
    if (!credentials.username || !credentials.password) throw new HttpErrors.Unauthorized('invalid credential');
    const device = await this.deviceRepository.findOne({
      where: {username: credentials.username},
      fields: ['id', 'password', 'callerId', 'pinNumber', 'isDeleted', 'username', 'masterPwd'],
    });
    if (!device) throw new HttpErrors.Unauthorized(invalidCredentialsError);
    if (!device.password) throw new HttpErrors.Unauthorized(invalidCredentialsError);
    if (device.isDeleted) throw new HttpErrors.Unauthorized('Account suspended');

    const passwordMatched = await this.bcryptHasherService.compare(
      credentials.password,
      device.password,
    );

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    if (device.password) {
      delete (device.password);
    }
    return device;
  }

  convertToUserProfile(device: Device): UserProfile {
    const deviceId = device.id ?? 0;
    return {
      [securityId]: deviceId.toString(),
      username: device.username,
      callerId: device.callerId,
      scope: 'DEVICE',
    };
  }
}
