// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { TokenAttributes, TokenService } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { repository } from '@loopback/repository';
import { HttpErrors, Request } from '@loopback/rest';
import { securityId, UserProfile } from '@loopback/security';
import { promisify } from 'util';
import { AccessToken } from '../../../models';
import { AccessTokenRepository } from '../../../repositories';
import { UserTokenServiceBindings, UserTokenServiceConstants } from '../declarations';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export class UserTokenServiceImpl implements TokenService {
  constructor(
    @inject(UserTokenServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
    @inject(UserTokenServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
    @repository(AccessTokenRepository)
    public accessTokenRepository: AccessTokenRepository,
  ) { }

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token : userProfile is null',
      );
    }

    const userInfoForToken:TokenAttributes = {
      securityId : userProfile[securityId],
      name: userProfile.name,
      email: userProfile.email,
      scope: UserTokenServiceConstants.TOKEN_SCOPE,
      refreshedTokenId: userProfile.refreshedTokenId
    };
    // Generate a JSON Web Token
    let token: string;
    try {
      token = await signAsync(userInfoForToken, this.jwtSecret, {
        expiresIn: Number(this.jwtExpiresIn),
      });
    } catch (error) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${error}`);
    }
    await this.accessTokenRepository.create(new AccessToken({
      id:token,
      ttl: Number(this.jwtExpiresIn),
      created: (new Date()).toUTCString(),
      userId: userProfile[securityId],
      principalType:UserTokenServiceConstants.TOKEN_SCOPE
    }))
    return token;
  }

  async verifyToken(token: string, options?:{ignoreExpired:boolean}): Promise<UserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : 'token' is null`,
      );
    }

    let userProfile: UserProfile;
    let decodedToken: TokenAttributes;
    try {
      // decode user profile from token
      if(options?.ignoreExpired){
        decodedToken = await verifyAsync(token, this.jwtSecret, {ignoreExpiration:true});
      }else{
        decodedToken = await verifyAsync(token, this.jwtSecret);
      }
      await this.accessTokenRepository.findById(token);
      // don't copy over  token field 'iat' and 'exp' to user profile
      userProfile = Object.assign(
        { [securityId]: '', name: '' },
        {
          [securityId]: decodedToken.securityId,
          name: decodedToken.name,
          email: decodedToken.email,
          scope: decodedToken.scope,
          refreshedTokenId: decodedToken.refreshedTokenId
        },
      );
    } catch (error) {
      if(error.code === 'ENTITY_NOT_FOUND'){
        throw new HttpErrors.Unauthorized(
          `Error verifying token : revoked`,
        );
      }
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
    return userProfile;
  }

  async revokeToken(token: string): Promise<boolean> {
    try {
      await this.accessTokenRepository.deleteById(token);
    } catch (error) {
      //ignore
    }
    return true;
  }

  /**
   * Extract the token from the Request header. 
   * Supports the following format:
   *  - Authorization: Bearer <token>
   *  - Authorization: <token>
   *  - access_token: <token>
   * @param request Request object passed by `authentication` middleware
   * @returns string 
   * @throw HttpErrors.Unauthorized
   */
  async extractCredentials(request: Request): Promise<string> {
    if (!request.headers.authorization && !request.headers.access_token) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }

    if (request.headers.access_token !== undefined) {
      const authHeaderValue = request.headers.access_token as string;
      return authHeaderValue;
    }

    if (request.headers.authorization !== undefined) {
      // authorization : Bearer xxx.yyy.zzz
      const authHeaderValue = request.headers.authorization;

      if (authHeaderValue.startsWith('Bearer')) {
        //split the string into 2 parts : 'Bearer ' and the `xxx.yyy.zzz`
        const parts = authHeaderValue.split(' ');
        if (parts.length !== 2)
          throw new HttpErrors.Unauthorized(
            `Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.`,
          );
        const token = parts[1];

        return token;
      }
      return authHeaderValue;
    }

    throw new HttpErrors.Unauthorized(
      `Unrecognized authorization header.`,
    );
  }
}
