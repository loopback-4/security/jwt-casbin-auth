import { 
  RefreshToken,
  RefreshTokenService, 
  TokenService 
} from '@loopback/authentication';
import { 
  BindingScope, 
  inject, 
  injectable, 
  generateUniqueId as uuid 
} from '@loopback/core';
import { repository } from '@loopback/repository';
import { HttpErrors } from '@loopback/rest';
import { 
  securityId, 
  UserProfile 
} from '@loopback/security';
import { promisify } from 'util';
import { UserRefreshTokenRepository } from '../../../repositories';
import {
  UserRefreshTokenServiceBindings,
  UserTokenServiceBindings,
  UserServiceBindings,
} from '../declarations';

import { UserServiceImpl } from './user.service.impl';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

@injectable({ scope: BindingScope.TRANSIENT })
export class UserRefreshTokenServiceImpl implements RefreshTokenService {
  constructor(
    @inject(UserRefreshTokenServiceBindings.REFRESH_SECRET)
    private refreshSecret: string,
    @inject(UserRefreshTokenServiceBindings.REFRESH_EXPIRES_IN)
    private refreshExpiresIn: string,
    @inject(UserRefreshTokenServiceBindings.REFRESH_ISSUER)
    private refreshIssure: string,
    @repository(UserRefreshTokenRepository)
    public refreshTokenRepository: UserRefreshTokenRepository,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserServiceImpl,
    @inject(UserTokenServiceBindings.TOKEN_SERVICE)
    public tokenService: TokenService,
  ) { }
  /**
   * Generate a refresh token, bind it with the given user profile + access
   * token, then store them in backend.
   */
  async generateToken( userProfile: UserProfile): Promise<RefreshToken> {
    const data = {
      token: uuid(),
    };
    const token = await signAsync(data, this.refreshSecret, {
      expiresIn: Number(this.refreshExpiresIn),
      issuer: this.refreshIssure,
    });
    const userRefreshToken = await this.refreshTokenRepository.create({
      userId: userProfile[securityId],
      refreshToken: token,
    });
    return {id: userRefreshToken.id!.toString(), token: token};
  }

  /**
   * Verify the validity of a refresh token, and make sure it exists in backend.
   * @param refreshToken
   */
   async verifyToken(userId: string, refreshToken: string): Promise<UserProfile> {
    try {
      const decodedToken = await verifyAsync(refreshToken, this.refreshSecret);
      await this.refreshTokenRepository.findOne({
        where:{
          and:[
            {refreshToken:refreshToken},
            {userId: userId}
          ]
        }
      });
      return Object.assign(
        { [securityId]: '', name: '' },
        {
          [securityId]: decodedToken.securityId,
          name: decodedToken.name,
          email: decodedToken.email,
          scope: decodedToken.scope
        },
      );
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
  }
  /*
   * Refresh the access token bound with the given refresh token.
   */
  async refreshToken(token: string, refreshToken: string): Promise<string> {
    try {
      let userProfile = await this.tokenService.verifyToken(token,{ignoreExpired:true});
      await this.verifyToken(userProfile[securityId], refreshToken);
      if(this.tokenService.revokeToken){
        this.tokenService.revokeToken(token);
      }
      // create a JSON Web Token based on the user profile
      return await this.tokenService.generateToken(userProfile);
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Token Refresh Error : ${error.message}`,
      );
    }
  }

  async revokeToken(refreshTokenId: string) {
    try {
        await this.refreshTokenRepository.deleteById(Number(refreshTokenId));
    } catch (e) {
      // ignore
    }
  }
}
