// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {TokenService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {HttpErrors, Request} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {promisify} from 'util';
import {DeviceTokenServiceBindings, DeviceTokenServiceConstants} from '../declarations';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export class DeviceTokenServiceImpl implements TokenService {
  constructor(
    @inject(DeviceTokenServiceBindings.TOKEN_SECRET) private jwtSecret: string,
    @inject(DeviceTokenServiceBindings.TOKEN_EXPIRES_IN) private jwtExpiresIn: string,
  ) {}
  
  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token : userProfile is null',
      );
    }
    const userInfoForToken = {
      id: userProfile[securityId],
      username: userProfile.username,
      callerId: userProfile.callerId,
      scope: DeviceTokenServiceConstants.TOKEN_SCOPE
    };
    // Generate a JSON Web Token
    let token: string;
    try {
      token = await signAsync(userInfoForToken, this.jwtSecret, {
        expiresIn: Number(this.jwtExpiresIn),
      });
    } catch (error) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${error}`);
    }

    return token;
  }

  async verifyToken(token: string): Promise<UserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : 'token' is null`,
      );
    }

    let userProfile: UserProfile;
    try {
      // decode user profile from token
      const decodedToken = await verifyAsync(token, this.jwtSecret,{ignoreExpiration:true});
      // don't copy over  token field 'iat' and 'exp' to user profile
      userProfile = Object.assign(
        {[securityId]: '', username: ''},
        {
          [securityId]: decodedToken.id,
          username: decodedToken.username,
          callerId: decodedToken.callerId,
          scope:DeviceTokenServiceConstants.TOKEN_SCOPE,
        },
      );
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
    return userProfile;
  }

  async extractCredentials(request: Request): Promise<string> {
    if (!request.headers.authorization && !request.headers.access_token) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }

    if (request.headers.access_token !== undefined) {
      const authHeaderValue = request.headers.access_token as string;
      return authHeaderValue;
    }

    if (request.headers.authorization !== undefined) {
      // authorization : Bearer xxx.yyy.zzz
      const authHeaderValue = request.headers.authorization;

      if (authHeaderValue.startsWith('Bearer')) {
        //split the string into 2 parts : 'Bearer ' and the `xxx.yyy.zzz`
        const parts = authHeaderValue.split(' ');
        if (parts.length !== 2)
          throw new HttpErrors.Unauthorized(
            `Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.`,
          );
        const token = parts[1];

        return token;
      }
      return authHeaderValue;
    }

    throw new HttpErrors.Unauthorized(
      `Unrecognized authorization header.`,
    );
  }
}
