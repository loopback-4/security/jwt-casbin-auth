// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  Credentials,
  UserService
} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {BcryptHasherBindings, BcryptHasherService} from '../..';
import {Role, User} from '../../../models';
import {RbacRepository, UserRepository} from '../../../repositories';
import {UserTokenServiceConstants} from '../declarations';


export class UserServiceImpl implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(RbacRepository)
    public rbacRepository: RbacRepository,
    @inject(BcryptHasherBindings.BCRYPT_HASHER_SERVICE)
    private bcryptHasherService: BcryptHasherService,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const invalidCredentialsError = 'Invalid credential.';
    if (!credentials.email || !credentials.password) throw new HttpErrors.BadRequest('Missing Email or Password');

    const user = await this.userRepository.findOne({
      where: {email: credentials.email},
      fields: ['id', 'password', 'cardId', 'staffId', 'name', 'phoneNumber', 'email', 'isDeleted'],
    });
    if (!user) throw new HttpErrors.Unauthorized(invalidCredentialsError);
    if (!user.password) throw new HttpErrors.Unauthorized(invalidCredentialsError);
    if (user.isDeleted) throw new HttpErrors.Unauthorized('Account suspended');

    const pwdMatched = await this.bcryptHasherService.compare(
      credentials.password,
      user.password,
    );
    if (!pwdMatched) throw new HttpErrors.Unauthorized(invalidCredentialsError);
    delete (user.password);
    return user;
  }

  async findUserById(id: number): Promise<User> {
    const userNotfound = 'invalid User';
    const foundUser = await this.userRepository.findOne({
      where: {id: id},
    });

    if (!foundUser) {
      throw new HttpErrors.Unauthorized(userNotfound);
    }
    return foundUser;
  }

  async findUserRoleById(userId: number): Promise<Role[]> {
    // console.log(`findUserRoleById : ${userId}`);
    const rbacs = await this.rbacRepository.find({
      where: {and: [{principalType: 'Enduser'}, {principalId: userId}]},
      include: ["role"]
    });
    // console.log(`rbacs : `, rbacs);

    let roles: Role[] = [];
    for (const rbac of rbacs) {
      const rbacJson = rbac.toJSON() as any;
      // console.log(`rbac : `, rbacJson);
      if (rbacJson.role) {
        let role = rbacJson.role;
        delete role.created;
        delete role.description;
        delete role.modified;
        roles.push(role);
      }
    }
    return roles;
  }

  async registerAdmin(user: User): Promise<User> {
    if (user.password) {
      user.password = await this.bcryptHasherService.hash(user.password);
    }
    return this.userRepository.create(user);
  }
  async registerOfficer(user: User): Promise<User> {
    throw new Error('Method not implemented.');
  }
  async registerMarshal(user: User): Promise<User> {
    throw new Error('Method not implemented.');
  }

  convertToUserProfile(user: User): UserProfile {
    return {
      [securityId]: user.id?.toString() ?? '',
      name: user.name,
      email: user.email,
      scope: UserTokenServiceConstants.TOKEN_SCOPE
    }
  }
}
