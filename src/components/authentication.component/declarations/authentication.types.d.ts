export declare module "@loopback/security" {
    export interface UserProfile {
        scope?: string,
        refreshedTokenId?: string
    }
}

export declare module "@loopback/authentication" {
    /**
     * A pre-defined type for user credentials. It assumes a user logs in
     * using the email and password. You can modify it if your app has different credential fields
     */
    export type Credentials = {
        username?: string,
        email?: string;
        password: string;
    };

    export type RefreshToken = {
        id: string,
        token: string,
    }

    export type TokenAttributes = {
        securityId: string,
        name?: string,
        email?: string,
        scope: string,
        refreshedTokenId?: string
    }

    /**
     * Merge with original `UserService<U,C> declaration`
     */
    export interface UserService<U, C> {
        findUserById(id: number): Promise<U>;
        findUserRoleById(userId: number): Promise<any>;
        registerAdmin(user: U): Promise<U>;
        registerOfficer(user: U): Promise<U>;
        registerMarshal(user: U): Promise<U>;
    }

    export interface TokenService {
        extractCredentials(request: Request): Promise<string>;
        verifyToken(userId: string, option?: {ignoreExpired: boolean}): Promise<UserProfile>;
    }

    /**
     * The token refresh service. An access token expires in limited time. Therefore
     * token refresh service is needed to keep replacing the old access token with
     * a new one periodically.
     */
    export interface RefreshTokenService {
        /**
         * Generate a refresh token, bind it with the given user profile + access
         * token, then store them in backend.
         */
        generateToken(userProfile: UserProfile): Promise<RefreshToken>;
        /**
         * Refresh the access token bound with the given refresh token.
         */
        refreshToken(token: string, refreshToken: string): Promise<string>;

        revokeToken(refreshToken: string);

        verifyToken(userId: string, refreshToken: string): Promise<U>;
    }
}

