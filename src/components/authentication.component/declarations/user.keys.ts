// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  Credentials,
  TokenService,
  UserService
} from '@loopback/authentication';
import { BindingKey } from '@loopback/core';
import { User } from '../../../models';
import {
  UserRefreshTokenServiceImpl
} from '../services';


export namespace UserTokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.user.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.user.jwt.expires.in.seconds',
  );
  export const TOKEN_SERVICE = BindingKey.create<TokenService>(
    'services.authentication.user.jwt.tokenservice',
  );
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<UserService<User, Credentials>>(
    'services.user.service',
  );
}

export namespace UserRefreshTokenServiceBindings {
  export const REFRESH_TOKEN_SERVICE = BindingKey.create<UserRefreshTokenServiceImpl>(
    'services.authentication.user.jwt.refresh.tokenservice',
  );
  export const REFRESH_SECRET = BindingKey.create<string>(
    'authentication.jwt.user.refresh.secret',
  );
  export const REFRESH_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.user.refresh.expires.in.seconds',
  );
  export const REFRESH_ISSUER = BindingKey.create<string>(
    'authentication.jwt.user.refresh.issuer',
  );
}
