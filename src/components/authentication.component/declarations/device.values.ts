export namespace DeviceTokenServiceConstants {
    export const TOKEN_SECRET_VALUE = 'myD3v1c3jwts3cr3t';
    export const TOKEN_EXPIRES_IN_VALUE = '21600';
    export const TOKEN_SCOPE = 'Device';
}