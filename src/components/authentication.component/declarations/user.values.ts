export namespace UserTokenServiceConstants {
    /**
     * The default secret used when generating token.
     */
    export const TOKEN_SECRET_VALUE = 'myjwts3cr3t';
    /**
     * The default expiration time for token.
     */
    export const TOKEN_EXPIRES_IN_VALUE = '21600';

    export const TOKEN_SCOPE = 'User';
}

export namespace UserRefreshTokenConstants {
    /**
     * The default secret used when generating refresh token.
     */
    export const REFRESH_SECRET_VALUE = 'r3fr35htok3n';
    /**
     * The default expiration time for refresh token.
     */
    export const REFRESH_EXPIRES_IN_VALUE = '216000';
    /**
     * The default issuer used when generating refresh token.
     */
    export const REFRESH_ISSUER_VALUE = 'loopback4';
}