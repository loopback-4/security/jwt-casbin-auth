export namespace AppTokenServiceConstants {
  /**
   * The default secret used when generating token.
   */
  export const TOKEN_SECRET_VALUE = 'appjwts3cr3t';
  /**
   * The default expiration time for token.
   */
  export const TOKEN_EXPIRES_IN_VALUE = '21600';

  export const TOKEN_SCOPE = 'App';
}