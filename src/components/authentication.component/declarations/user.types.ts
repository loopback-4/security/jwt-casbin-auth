export type UserTokenInfo = {
    id: string,
    name: string,
    email: string,
    scope: string,
    refreshTokenId?:string,
}