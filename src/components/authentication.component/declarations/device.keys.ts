// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/authentication-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { Credentials, TokenService, UserService } from '@loopback/authentication';
import { BindingKey } from '@loopback/core';
import { Device } from '../../../models';



export namespace DeviceTokenServiceBindings {
    export const TOKEN_SECRET = BindingKey.create<string>(
        'authentication.device.jwt.secret',
    );
    export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
        'authentication.device.jwt.expires.in.seconds',
    );
    export const TOKEN_SERVICE = BindingKey.create<TokenService>(
        'services.authentication.device.jwt.tokenservice',
    );
}

export namespace DeviceServiceBindings {
    export const DEVICE_SERVICE = BindingKey.create<UserService<Device, Credentials>>(
        'services.device.service',
    );
}