import { AuthorizationTags } from "@loopback/authorization";
import { Binding, Component } from "@loopback/core";
import { AuthorizerBindings, VoterBindings } from "./declarations";
import { CasbinAuthorizer } from "./authorizer";
import { UserOwnerResolverVoter } from "./voters";

export class RbacAuthorizationComponent implements Component {
    bindings: Binding[] = [
        Binding.bind(
            AuthorizerBindings.CASBIN_AUTHORIZER
        ).toProvider(CasbinAuthorizer)
        .tag(AuthorizationTags.AUTHORIZER),
        Binding.bind(
            VoterBindings.USER_OWNER_RESOLVER
        ).toProvider(UserOwnerResolverVoter),
    ]
}