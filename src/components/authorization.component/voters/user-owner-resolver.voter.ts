import { Provider } from '@loopback/core';
import {
    AuthorizationContext,
    AuthorizationDecision,
    AuthorizationMetadata,
    Authorizer,
} from '@loopback/authorization';
import {
    SecurityBindings,
    UserProfile,
    securityId
} from '@loopback/security';
import { IS_OWNER, OWNER } from '../declarations';


// Class level authorizer
export class UserOwnerResolverVoter implements Provider<Authorizer> {
    constructor() { }
    /**
   * @returns authenticateFn
   */
    value(): Authorizer {
        return this.authorize.bind(this);
    }

    async authorize(
        authorizationCtx: AuthorizationContext,
        metadata: AuthorizationMetadata,
    ): Promise<AuthorizationDecision> {
        if (metadata.allowedRoles?.includes(OWNER)) {
            const resourceId = authorizationCtx.invocationContext.args[0];
            const user = await authorizationCtx.invocationContext.get<UserProfile>(SecurityBindings.USER, {
              optional: true,
            });
            if (!user) {
              return AuthorizationDecision.DENY;
            }
            const userId = user[securityId];
            if (userId === resourceId) {
              authorizationCtx.invocationContext.bind(IS_OWNER).to(true);
            }
          }
          return AuthorizationDecision.ABSTAIN;
    }
}