import { Authorizer } from "@loopback/authorization";
import { BindingKey } from "@loopback/core";

export namespace AuthorizerBindings {
    export const CASBIN_AUTHORIZER = BindingKey.create<Authorizer>(
        'authorizer.casbin',
    );
}

export namespace VoterBindings {
    export const USER_OWNER_RESOLVER = BindingKey.create<Authorizer>(
        'authorizer.user-owner-resolver.voter',
    );
}

export const RESOURCE_ID = BindingKey.create<string>('resourceId');
export const IS_OWNER = BindingKey.create<boolean>('isOwner');

