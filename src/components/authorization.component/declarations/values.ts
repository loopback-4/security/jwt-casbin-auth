export const ROOT = "system";
export const OWNER = "$owner";
export const ADMIN = "admin";
export const OFFICER = "officer";
export const MARSHAL = "marshal";
export const GADGET = "gadget";
export const NONE = "none";

export const ENDUSER = "Enduser";
export const DEVICE = "Device";
export const APPLICATION = "Application";
