import {
    ANONYMOUS,
    AUTHENTICATED,
    AuthorizationContext,
    AuthorizationDecision,
    AuthorizationMetadata,
    AuthorizationRequest,
    Authorizer,
    EVERYONE,
    UNAUTHENTICATED
} from '@loopback/authorization';
import {Provider} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
    SecurityBindings, securityId, UserProfile
} from '@loopback/security';
import * as casbin from 'casbin';
import {Enforcer} from 'casbin';
import path from 'path';
import {Role} from '../../../models';
import {RbacRepository} from '../../../repositories';
import {
    AppTokenServiceConstants,
    DeviceTokenServiceConstants,
    UserTokenServiceConstants
} from '../../authentication.component';
import {APPLICATION, DEVICE, ENDUSER, IS_OWNER, OWNER, RESOURCE_ID, ROOT} from '../declarations';

// Class level authorizer
export class CasbinAuthorizer implements Provider<Authorizer> {
    constructor(
        @repository(RbacRepository)
        private rbacRepository: RbacRepository
    ) { }
    /**
   * @returns authenticateFn
   */
    value(): Authorizer {
        return this.authorize.bind(this);
    }

    async authorize(
        authorizationCtx: AuthorizationContext,
        metadata: AuthorizationMetadata,
    ): Promise<AuthorizationDecision> {
        // console.log(`metadata : ${JSON.stringify(metadata, null, 4)}`);
        let allowedRoles = metadata.allowedRoles;
        if (!allowedRoles) {
            allowedRoles = [ROOT];
        }

        const authenticated = authorizationCtx.principals.length > 0;

        if (!authenticated) {
            for (const role of allowedRoles) {
                if (role === EVERYONE || role === ANONYMOUS || role === UNAUTHENTICATED) {
                    return AuthorizationDecision.ALLOW;
                }
            }
            return AuthorizationDecision.DENY;
        } else {
            const user = await authorizationCtx.invocationContext.get<UserProfile>(SecurityBindings.USER, {
                optional: true,
            });
            if (!user) {
                return AuthorizationDecision.DENY;
            }

            const resourceId = await authorizationCtx.invocationContext.get(
                RESOURCE_ID,
                {optional: true},
            );
            const isOwner = await authorizationCtx.invocationContext.get(
                IS_OWNER,
                {optional: true},
            );

            const object = resourceId ?? metadata.resource ?? authorizationCtx.resource;

            const request: AuthorizationRequest = {
                subject: `u${user[securityId]}`,
                object,
                action: metadata.scopes?.[0] ?? 'execute',
            };
            const enforcer = await this.createEnforcer(
                user,
                allowedRoles,
                object,
                metadata.scopes,
                isOwner
            );

            const policy = await enforcer.getPolicy();
            const groupingPolicy = await enforcer.getGroupingPolicy();
            console.log(`request : \n${JSON.stringify(request, null, 4)}`);
            console.log(`policy :`);
            for (let p of policy) {
                console.log(` p,${p}`);
            }
            for (let g of groupingPolicy) {
                console.log(` g,${g}`);
            }
            let allowedByRole = await enforcer.enforce(
                request.subject,
                request.object,
                request.action,
            );
            if (allowedByRole) {
                return AuthorizationDecision.ALLOW;
            }
            return AuthorizationDecision.DENY;
        }
    }

    async createEnforcer(
        user: UserProfile,
        allowedRoles: string[],
        resource?: string,
        action?: string[],
        isOwner?: boolean,
    ): Promise<Enforcer> {
        const conf = path.resolve(
            __dirname,
            '../../../../casbin/casbin_rbac_model.conf',
        );
        const enforcer = await casbin.newEnforcer(conf);

        const act = action ? action[0] : 'execute';
        for (const sub of allowedRoles) {
            if (sub !== ROOT)
                enforcer.addPolicy(sub, resource ?? '*', act);
        }
        enforcer.addPolicy(ROOT, resource ?? '*', act);

        enforcer.addRoleForUser(`u${user[securityId]}`, AUTHENTICATED);
        enforcer.addRoleForUser(`u${user[securityId]}`, UNAUTHENTICATED);
        enforcer.addRoleForUser(`u${user[securityId]}`, EVERYONE);
        enforcer.addRoleForUser(`u${user[securityId]}`, ANONYMOUS);

        let type = ENDUSER;
        if (user.scope) {
            if (user.scope === UserTokenServiceConstants.TOKEN_SCOPE) {
                type = ENDUSER;
            }
            if (user.scope === DeviceTokenServiceConstants.TOKEN_SCOPE) {
                type = DEVICE;
            }
            if (user.scope === AppTokenServiceConstants.TOKEN_SCOPE) {
                type = APPLICATION;
            }
        }
        const rbacs = await this.rbacRepository.find({
            where: {
                principalId: Number(user[securityId]),
                principalType: type,
            },
            include: [
                {
                    relation: 'role'
                }
            ]
        });
        rbacs.forEach((item) => {
            const rbac = item.toJSON() as any;
            let role: Role;
            if (rbac.role) {
                role = rbac.role as Role;
                enforcer.addRoleForUser(`u${user[securityId]}`, role.name);
            }
        });
        if (isOwner) {
            enforcer.addRoleForUser(`u${user[securityId]}`, OWNER);
        }
        return enforcer;
    };
}
