import { 
   Binding, 
   Component 
} from "@loopback/core";
import { 
   BcryptHasherBindings, 
   BcryptHasherService 
} from ".";

export class BcryptHasherComponent implements Component {
   bindings?: Binding[] = [
    Binding.bind(BcryptHasherBindings.BCRYPT_HASHER_SERVICE).toClass(BcryptHasherService),
    Binding.bind(BcryptHasherBindings.ROUNDS).to(10),
   ]; 
}