import { BindingKey } from "@loopback/core";
import { BcryptHasher } from "./types";

export namespace BcryptHasherBindings {
    export const BCRYPT_HASHER_SERVICE = BindingKey.create<BcryptHasher>(
      'services.bcrypt.hasher',
    );
    export const ROUNDS = BindingKey.create<number>(
      'services.bcrypt.hasher.rounds'
    );
  }