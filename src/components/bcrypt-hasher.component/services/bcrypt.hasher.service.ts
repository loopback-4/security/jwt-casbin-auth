import { 
    BindingScope, 
    inject, 
    injectable 
} from "@loopback/core";
import { 
    compare, 
    genSalt, 
    hash 
} from 'bcryptjs';
import { 
    BcryptHasherBindings, 
    BcryptHasher 
} from "../declaration";

@injectable({ scope: BindingScope.SINGLETON })
export class BcryptHasherService implements BcryptHasher<string>  {
    @inject(BcryptHasherBindings.ROUNDS)
    public readonly rounds: number

    async hash(clearTxt: string): Promise<string> {
        const salt = await genSalt(this.rounds);
        return await hash(clearTxt, salt);
    }
    async compare(clearTxt: string, hashedTxt: string): Promise<boolean> {
        const matches = await compare(clearTxt, hashedTxt);
        return matches;
    }
}