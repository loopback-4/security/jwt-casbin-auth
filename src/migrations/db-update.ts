import {WebServiceApplication} from '../application';

export async function db_update() {
  console.log('Migrating schemas (alter existing schema)');

  const app = new WebServiceApplication();
  await app.boot();
  await app.migrateSchema({existingSchema: 'alter'});
  console.log('Migration completed.');
  // Connectors usually keep a pool of opened connections,
  // this keeps the process running even after all work is done.
  // We need to exit explicitly.
  process.exit(0);
}

db_update().catch(err => {
  console.error('Cannot auto-update database schema', err);
  process.exit(1);
});
