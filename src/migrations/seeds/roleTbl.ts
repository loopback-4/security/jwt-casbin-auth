export const roleTbl = [
  {
    name: 'system',
    description: 'Root system admin'
  },
  {
    name: 'admin',
    description: 'Site admin'
  },
  {
    name: 'officer',
    description: 'Site officer'
  },
  {
    name: 'marshal',
    description: 'Site marshal'
  },
  {
    name: 'device',
    description: 'Site device'
  },
  {
    name: 'application',
    description: 'External application'
  },
];
