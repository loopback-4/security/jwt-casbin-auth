import {WebServiceApplication} from '../application';
import {BcryptHasherService} from '../components';
import {Role} from '../models';
import {RbacRepository, RoleRepository, UserRepository} from '../repositories';
import {roleTbl, usersTbl} from './seeds';


export async function db_populate() {
  console.log('Populating database with seed data');

  const app = new WebServiceApplication();
  await app.boot();

  const userRepository = await app.getRepository(UserRepository);
  const roleRepository = await app.getRepository(RoleRepository);
  const rbacRepository = await app.getRepository(RbacRepository);

  let roles = {} as {[key: string]: Role};
  //Seed the database
  if (roleTbl && roleTbl?.length > 0) {
    for (const r of roleTbl) {
      const _r = await roleRepository.create(r);
      roles[_r.name] = _r;
    }
  }
  // console.log(role);
  if (usersTbl && usersTbl?.length > 0) {
    const hasher = new BcryptHasherService();
    for (const u of usersTbl) {
      let {role, ...usr} = u;
      const pwd = await hasher.hash(u.password);
      let user = await userRepository.create(
        Object.assign(usr, {password: pwd})
      );
      // console.log(user);
      await rbacRepository.create({
        principalId: user.id,
        principalType: 'Enduser',
        roleId: roles[role].id
      });
    }
  }

  console.log('Population completed.');
  // Connectors usually keep a pool of opened connections,
  // this keeps the process running even after all work is done.
  // We need to exit explicitly.
  process.exit(0);
}

db_populate().catch(err => {
  console.error('Failed to populate database', err);
  process.exit(1);
});
