import {WebServiceApplication} from '../application';

export async function db_migrate() {
  console.log('Migrating schemas (drop existing schema)');

  const app = new WebServiceApplication();
  await app.boot();
  await app.migrateSchema({existingSchema: 'drop'});
  console.log('Migration completed.');
  // Connectors usually keep a pool of opened connections,
  // this keeps the process running even after all work is done.
  // We need to exit explicitly.
  process.exit(0);
}

db_migrate().catch(err => {
  console.error('Cannot auto-migrate database schema', err);
  process.exit(1);
});
