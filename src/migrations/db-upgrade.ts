import {WebServiceApplication} from '../application';

export async function db_upgrade(args: string[]) {
  if (args.length !== 3) {
    throw new Error('Invalid syntax');
  }
  console.log(`Data migration to ${args[2]}`)
  const script = await import(`../migrations/scripts/${args[2]}`);
  const app = new WebServiceApplication();
  await app.boot();
  await script.migrate(app);
  console.log('Migration completed.');
  // Connectors usually keep a pool of opened connections,
  // this keeps the process running even after all work is done.
  // We need to exit explicitly.
  process.exit(0);
}

db_upgrade(process.argv).catch(err => {
  console.error('Cannot auto-migrate database schema', err);
  process.exit(1);
});
