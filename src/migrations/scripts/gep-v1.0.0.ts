// gep-v1.0.0 : migrate GEP database from LB3 to LB4
import {WebServiceApplication} from '../../application';
import {RoleRepository} from '../../repositories';

export async function migrate(app: WebServiceApplication) {
  await app.migrateSchema({existingSchema: 'alter'});

  const roleRepository = await app.getRepository(RoleRepository);
  // add new role (application)
  await roleRepository.create({name: 'application', description: 'External app'});
}
