import { 
    getModelSchemaRef, 
    RequestBodyObject,  
    ResponseObject 
} from "@loopback/rest";
import { 
    Role, 
    User 
} from "../../models";

export const LoginResponseObject: ResponseObject = {
    description: 'Login Response',
    content: {
        'application/json': {
            schema: {
                type: 'object',
                title: 'LoginResponse',
                properties: {
                    accessToken: { type: 'string' },
                    refreshToken:{type: 'string'},
                    user: getModelSchemaRef(User, {
                        exclude: [
                            'password',
                            'verificationToken',
                            'realm',
                            'isDeleted',
                            'isGroup',
                            'createdAt',
                            'updatedAt',
                            'emailVerified'
                        ]
                    }),
                    roles: {
                        type: 'array',
                        items: getModelSchemaRef(
                            Role, {
                            exclude: ['description', 'created', 'modified']
                        }
                        )
                    }
                },
            },
        },
    },
}
export const LoginRequestBodyObject: RequestBodyObject = {
    description: 'Login credential request body',
    required: true,
    content: {
        'application/json':
        {
            schema: {
                type: 'object',
                title: 'Credential',
                properties: {
                    username: { type: 'string' },
                    email: { type: 'string' },
                    password: { type: 'string' },
                }
            }
        }
    },
};

export const LogoutResponseObject: ResponseObject = {
    description: 'Logout Response',
    content: {
        'application/json': {
            schema: {
                type: 'object',
                title: 'LoginResponse',
                properties: {
                    status: { type: 'string' },
                },
            },
        },
    },
}



