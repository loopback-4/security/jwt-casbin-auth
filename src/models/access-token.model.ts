import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    mysql: {table: "SystemToken"}
  }
})
export class AccessToken extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    length: 512,
    mysql: {
      datatype: 'varchar',
      dataLength: 512,
    }
  })
  id: string;

  @property({
    type: 'number',
    default: -1,
  })
  ttl?: number;

  @property({
    type: 'string',
  })
  scopes?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      type: 'datetime'
    }
  })
  created?: string;

  @property({
    type: 'string',
    required: true,
  })
  principalType: string;

  @property({
    type: 'string',
  })
  userId?: string;

  constructor(data?: Partial<AccessToken>) {
    super(data);
  }
}

export interface AccessTokenRelations {
  // describe navigational properties here
}

export type AccessTokenWithRelations = AccessToken & AccessTokenRelations;
