import {Entity, model, property} from '@loopback/repository';

@model()
export class Device extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'string',
    // index: {unique: true}
  })
  callerId?: string;

  @property({
    type: 'number',
  })
  pinNumber?: number;

  @property({
    type: 'boolean',
    default: false,
  })
  isDeleted?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      type: 'datetime'
    }
  })
  createdAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      type: 'datetime',
    }
  })
  updatedAt?: string;

  @property({
    type: 'number',
  })
  masterPwd?: number;

  @property({
    type: 'string',
  })
  realm?: string;

  @property({
    type: 'string',
    required: true,
    index: {unique: true}
  })
  username: string;

  @property({
    type: 'boolean',
  })
  emailVerified?: boolean;

  @property({
    type: 'string',
  })
  verificationToken?: string;


  constructor(data?: Partial<Device>) {
    super(data);
  }
}

export interface DeviceRelations {
  // describe navigational properties here
}

export type DeviceWithRelations = Device & DeviceRelations;
