import {Entity, hasMany, model, property} from '@loopback/repository';
import {Rbac} from './rbac.model';

@model({
  settings: {
    mysql: {table: "Role"},
  }
})
export class Role extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    index: {unique: true}
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      type: 'datetime'
    }
  })
  created?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      type: 'datetime'
    }
  })
  modified?: string;

  @hasMany(() => Rbac)
  rbacs: Rbac[];

  constructor(data?: Partial<Role>) {
    super(data);
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations;
