import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Role} from './role.model';
import {User} from './user.model';

@model({
  settings: {
    mysql: {table: "RoleMapping"}
  }
})
export class Rbac extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    default: "EndUser",
  })
  principalType?: string;

  @belongsTo(() => User, {name: 'user'})
  principalId: number;

  @belongsTo(() => Role, {name: 'role'})
  roleId: number;

  constructor(data?: Partial<Rbac>) {
    super(data);
  }
}

export interface RbacRelations {
  // describe navigational properties here
}

export type RbacWithRelations = Rbac & RbacRelations;
