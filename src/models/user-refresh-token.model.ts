import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

@model()
export class UserRefreshToken extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    length: 512,
    required: true,
    mysql:{
      datatype:'varchar',
      dataLength:512
    }
  })
  refreshToken: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<UserRefreshToken>) {
    super(data);
  }
}

export interface UserRefreshTokenRelations {
  // describe navigational properties here
}

export type UserRefreshTokenWithRelations = UserRefreshToken & UserRefreshTokenRelations;
