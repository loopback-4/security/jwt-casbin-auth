import {Entity, hasOne, model, property} from '@loopback/repository';
import {UserRefreshToken} from './user-refresh-token.model';

@model({
  settings: {
    mysql: {table: "Enduser"}
  }
})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true
  })
  id?: number;

  @property({
    type: 'string',
    hidden: true,
  })
  password?: string;

  @property({
    type: 'string',
    index: {unique: true}
  })
  cardId?: string;

  @property({
    type: 'string',
    index: {unique: true}
  })
  staffId?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  phoneNumber?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isDeleted?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isGroup?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      dataType: 'datetime',
    },
  })
  createdAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
    mysql: {
      dataType: 'datetime',
    },
  })
  updatedAt?: string;

  @property({
    type: 'string',
  })
  realm?: string;

  @property({
    type: 'string',
    required: true,
    index: {unique: true}
  })
  email: string;

  @property({
    type: 'boolean',
  })
  emailVerified?: boolean;

  @property({
    type: 'string',
    hidden: true,
  })
  verificationToken?: string;

  @hasOne(() => UserRefreshToken)
  refreshToken: UserRefreshToken;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
