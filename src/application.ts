import {BootMixin} from '@loopback/boot';
import {
  ApplicationConfig, createBindingFromClass, 
  // createBindingFromClass
} from '@loopback/core';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {
  RestApplication,
} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {
  AuthenticationComponent, 
} from '@loopback/authentication';
import {MySequence} from './sequence';

import { 
  JwtAuthenticationUserComponent,  
  JwtAuthenticationDeviceComponent, 
  SecuritySpecEnhancer
} from './components/authentication.component';
import {
  RbacAuthorizationComponent,
} from './components/authorization.component';

import { 
  AuthorizationBindings, 
  AuthorizationComponent, 
  AuthorizationDecision, 
} from '@loopback/authorization';
import { BcryptHasherComponent } from './components';

export {ApplicationConfig};

export class WebServiceApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    //  Authentication & Authorization -------------------------------------------
    this.component(BcryptHasherComponent);
    // Authentication components
    this.component(AuthenticationComponent);
    this.component(JwtAuthenticationUserComponent);
    this.component(JwtAuthenticationDeviceComponent);
    // Authorization components
    this.configure(AuthorizationBindings.COMPONENT).to({
      precedence: AuthorizationDecision.DENY,
      defaultDecision: AuthorizationDecision.DENY,
    });
    this.component(AuthorizationComponent);
    this.component(RbacAuthorizationComponent);
    // Enhance OpenApi spec
    this.add(createBindingFromClass(SecuritySpecEnhancer));
    // ---------------------------------------------------------------------------

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }
}
