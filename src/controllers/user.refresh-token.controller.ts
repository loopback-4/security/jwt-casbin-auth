import { inject } from '@loopback/core';

import {
  get,
  param,
  Request,
  RestBindings,
} from '@loopback/rest';
import { 
  RefreshTokenService,  
  TokenService 
} from '@loopback/authentication';
import { 
  UserRefreshTokenServiceBindings,
  UserTokenServiceBindings
} from '../components/authentication.component';

export class UserRefreshTokenController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private request: Request,
    @inject(UserTokenServiceBindings.TOKEN_SERVICE) private tokenService: TokenService,
    @inject(UserRefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE) private refeshTokenService: RefreshTokenService,
  ) { }

  @get('/users/refresh-token/{token}', {
    responses: {
      '200': {
        description: 'Get new Access Token',
        content: {
          'application/json': {
            schema:{
              type:'object',
              title:'NewAccessToken',
              properties:{
                accessToken:{type:'string'},
              }
            }
          }
        },
      },
    },
  })
  async refresh(
    @param.path.string('token', {required:true}) token:string,
  ): Promise<object> {
    const expiredToken = await this.tokenService.extractCredentials(this.request);
    const newToken = await this.refeshTokenService.refreshToken(expiredToken, token);
    return {
      accessToken:newToken
    };
  }
}
