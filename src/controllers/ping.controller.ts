import {authenticate} from '@loopback/authentication';
import {
  AUTHENTICATED,
  authorize, UNAUTHENTICATED
} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {
  get, Request, response, RestBindings
} from '@loopback/rest';
import {NONE} from '../components/authorization.component';
import {PingResponseObject} from '../configurations/specs';

/**
 * A simple controller to bounce back http requests
 */
@authenticate('jwt-user')
@authorize({
  allowedRoles: [NONE],
  resource: 'ping',
})
export class PingController {
  constructor(
    @inject(RestBindings.Http.REQUEST) private req: Request,
  ) { }

  @authenticate.skip()
  @authorize.skip()
  @get('/ping')
  @response(200, PingResponseObject)
  ping(): object {
    // Reply with a greeting, the current time, the url, and request headers
    return {
      greeting: 'Hello from LoopBack',
      date: new Date(),
      url: this.req.url,
      headers: Object.assign({}, this.req.headers),
    };
  }


  @authenticate.skip()
  @authorize({
    allowedRoles: [UNAUTHENTICATED],
    scopes: ['pingUnauthed'],
    resource: 'ping',
  })
  @get('/ping/unauthed')
  pingUnauthed() {
    return {message: 'unauthed: success'};
  }

  @authorize({
    allowedRoles: [AUTHENTICATED],
    scopes: ['pingAuthed'],
    resource: 'ping',
  })
  @get('/ping/authed')
  pingAuthed() {
    return {message: 'authenticated: success'};
  }
}
