import {
  authenticate,
  Credentials,
  RefreshTokenService,
  TokenService,
  UserService
} from '@loopback/authentication';
import {
  AUTHENTICATED,
  authorize
} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, Request, requestBody,
  response, RestBindings
} from '@loopback/rest';
import {BcryptHasherBindings, BcryptHasherService} from '../components';
import {
  UserRefreshTokenServiceBindings, UserServiceBindings, UserTokenServiceBindings
} from '../components/authentication.component';
import {ADMIN, NONE, OWNER, VoterBindings} from '../components/authorization.component';
import {
  LoginRequestBodyObject,
  LoginResponseObject,
  LogoutResponseObject
} from '../configurations/specs';
import {User} from '../models';
import {UserRepository} from '../repositories';

@authenticate('jwt-user')
@authorize({
  allowedRoles: [NONE],
  resource: 'user',
  scopes: ['*']
})
export class UserController {
  constructor(
    @repository(UserRepository)
    private userRepository: UserRepository,
    @inject(RestBindings.Http.REQUEST)
    private request: Request,
    @inject(UserServiceBindings.USER_SERVICE)
    private userService: UserService<User, Credentials>,
    @inject(UserTokenServiceBindings.TOKEN_SERVICE)
    private tokenService: TokenService,
    @inject(UserRefreshTokenServiceBindings.REFRESH_TOKEN_SERVICE)
    private refreshTokenService: RefreshTokenService,
    @inject(BcryptHasherBindings.BCRYPT_HASHER_SERVICE)
    private bcryptHasherService: BcryptHasherService,
  ) { }

  @authenticate.skip()
  @authorize.skip()
  @post('/users/login')
  @response(200, LoginResponseObject)
  async login(
    @requestBody(LoginRequestBodyObject) credentials: Credentials
  ) {
    const user = await this.userService.verifyCredentials(credentials);
    const roles = await this.userService.findUserRoleById(user.id!);

    let profile = this.userService.convertToUserProfile(user);
    const refreshToken = await this.refreshTokenService.generateToken(profile);
    const accessToken = await this.tokenService.generateToken({
      ...profile,
      refreshedTokenId: refreshToken.id
    });

    return {
      accessToken: accessToken,
      refreshToken: refreshToken.token,
      user: user,
      roles: roles
    };
  }

  @authorize({
    allowedRoles: [AUTHENTICATED],
    resource: 'user',
    scopes: ['logout']
  })
  @post('/users/logout')
  @response(200, LogoutResponseObject)
  async logout() {
    try {
      const token = await this.tokenService.extractCredentials(this.request);
      const profile = await this.tokenService.verifyToken(token, {ignoreExpired: true});
      if (this.tokenService.revokeToken)
        await this.tokenService.revokeToken(token);
      if (profile.refreshedTokenId)
        await this.refreshTokenService.revokeToken(profile.refreshedTokenId);
      return {
        status: "SUCCESS"
      };
    } catch (e) {
      return {
        status: `EALREADY: ${e.message}`
      };
    }
  }

  @authorize({
    allowedRoles: [NONE],
    resource: 'ping',
  })
  @post('/users/admin')
  @response(200, {
    description: 'User model instance',
    content: {'application/json': {schema: getModelSchemaRef(User, {exclude: ['password']})}},
  })
  async createAdmin(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
  ): Promise<User> {
    if (user.password) {
      user.password = await this.bcryptHasherService.hash(user.password);
    }
    return this.userRepository.create(user);
  }

  @get('/users/count')
  @response(200, {
    description: 'User model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.count(where);
  }

  @get('/users')
  @response(200, {
    description: 'Array of User model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(User, {includeRelations: false}),
        },
      },
    },
  })
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    if (filter?.include) {
      const include = [];
      for (const inc of filter.include) {
        if (typeof inc === 'string') {
          if (inc !== 'rbacs' && inc !== 'accessTokens') {
            include.push(inc);
          }
          continue;
        }
        if (typeof inc === 'object') {
          if (inc.relation !== 'rbacs' && inc.relation !== 'accessTokens') {
            include.push(inc);
          }
          continue;
        }
      }
      filter.include = include;
    }
    return this.userRepository.find(filter);
  }

  @patch('/users')
  @response(200, {
    description: 'User PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where);
  }

  @authorize({
    allowedRoles: [OWNER, ADMIN],
    resource: 'users',
    scopes: ['findById'],
    voters: [VoterBindings.USER_OWNER_RESOLVER],
  })
  @get('/users/{id}')
  @response(200, {
    description: 'User model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(
          User, {
          includeRelations: true
        }
        ),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(User, {exclude: ['where']}) filter?: FilterExcludingWhere<User>
  ): Promise<User> {
    if (filter?.include) {
      const include = [];
      for (const inc of filter.include) {
        if (typeof inc === 'string') {
          if (inc !== 'rbacs' && inc !== 'accessTokens') {
            include.push(inc);
          }
          continue;
        }
        if (typeof inc === 'object') {
          if (inc.relation !== 'rbacs' && inc.relation !== 'accessTokens') {
            include.push(inc);
          }
          continue;
        }
      }
      filter.include = include;
    }
    return this.userRepository.findById(id, filter);
  }

  @patch('/users/{id}')
  @response(204, {
    description: 'User PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    await this.userRepository.updateById(id, user);
  }

  @put('/users/{id}')
  @response(204, {
    description: 'User PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.replaceById(id, user);
  }

  @del('/users/{id}')
  @response(204, {
    description: 'User DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userRepository.deleteById(id);
  }
}
