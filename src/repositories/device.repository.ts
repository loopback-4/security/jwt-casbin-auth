import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MariadbDataSource} from '../datasources';
import {Device, DeviceRelations} from '../models';

export class DeviceRepository extends DefaultCrudRepository<
  Device,
  typeof Device.prototype.id,
  DeviceRelations
> {
  constructor(
    @inject('datasources.mariadb') dataSource: MariadbDataSource,
  ) {
    super(Device, dataSource);
  }
}
