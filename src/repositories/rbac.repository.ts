import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {MariadbDataSource} from '../datasources';
import {Rbac, RbacRelations, Role, User} from '../models';
import {RoleRepository} from './role.repository';
import {UserRepository} from './user.repository';

export class RbacRepository extends DefaultCrudRepository<
  Rbac,
  typeof Rbac.prototype.id,
  RbacRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Rbac.prototype.id>;
  public readonly role: BelongsToAccessor<Role, typeof Rbac.prototype.id>;

  constructor(
    @inject('datasources.mariadb')
    dataSource: MariadbDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('RoleRepository')
    protected roleRepositoryGetter: Getter<RoleRepository>,
  ) {
    super(Rbac, dataSource);
    this.role = this.createBelongsToAccessorFor('role', roleRepositoryGetter,);
    this.registerInclusionResolver('role', this.role.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
