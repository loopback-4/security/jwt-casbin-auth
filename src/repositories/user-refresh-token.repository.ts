import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {MariadbDataSource} from '../datasources';
import {UserRefreshToken, UserRefreshTokenRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class UserRefreshTokenRepository extends DefaultCrudRepository<
  UserRefreshToken,
  typeof UserRefreshToken.prototype.id,
  UserRefreshTokenRelations
> {

  public readonly user: BelongsToAccessor<User, typeof UserRefreshToken.prototype.id>;

  constructor(
    @inject('datasources.mariadb') dataSource: MariadbDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(UserRefreshToken, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
