import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {MariadbDataSource} from '../datasources';
import {User, UserRelations, Rbac, UserRefreshToken, AccessToken} from '../models';
import {RbacRepository} from './rbac.repository';
import {UserRefreshTokenRepository} from './user-refresh-token.repository';
import {AccessTokenRepository} from './access-token.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly refreshToken: HasOneRepositoryFactory<UserRefreshToken, typeof User.prototype.id>;

  constructor(
    @inject('datasources.mariadb') 
    dataSource: MariadbDataSource,
    @repository.getter('RbacRepository') 
    protected rbacRepositoryGetter: Getter<RbacRepository>, @repository.getter('RefreshTokenRepository') protected refreshTokenRepositoryGetter: Getter<UserRefreshTokenRepository>, @repository.getter('AccessTokenRepository') protected accessTokenRepositoryGetter: Getter<AccessTokenRepository>,
  ) {
    super(User, dataSource);
    this.refreshToken = this.createHasOneRepositoryFactoryFor('refreshToken', refreshTokenRepositoryGetter);
    this.registerInclusionResolver('refreshToken', this.refreshToken.inclusionResolver);
  }
}
