import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {MariadbDataSource} from '../datasources';
import {Role, RoleRelations, Rbac} from '../models';
import {RbacRepository} from './rbac.repository';

export class RoleRepository extends DefaultCrudRepository<
  Role,
  typeof Role.prototype.id,
  RoleRelations
> {

  public readonly rbacs: HasManyRepositoryFactory<Rbac, typeof Role.prototype.id>;

  constructor(
    @inject('datasources.mariadb') 
    dataSource: MariadbDataSource, 
    @repository.getter('RbacRepository') 
    protected rbacRepositoryGetter: Getter<RbacRepository>,
  ) {
    super(Role, dataSource);
    this.rbacs = this.createHasManyRepositoryFactoryFor('rbacs', rbacRepositoryGetter,);
    this.registerInclusionResolver('rbacs', this.rbacs.inclusionResolver);
  }
}
